#!/usr/bin/python

############################################ 
#                                          #
#   Write by: Christian Contreras-Campana  #
#   email: christian.contreras@desy.de     #
#   Date: 21.06.2015                       #
#                                          #
############################################

# Description: The following code converts DESY ttH (also Top) analysis text table into latex table format

import sys
import argparse
import re
from os import listdir
import glob
from collections import defaultdict

# Command line parsing options
parser = argparse.ArgumentParser(description='Usage: python EventYieldToLatexConverter.py --CateN "*" --Merge True --Channel mumu --Normalized lumiCorrected')
parser.add_argument('--DirPath', metavar='C', type=str,  default="EventYield",    help='Directory path')
parser.add_argument('--CateN',   metavar='N', type=str,  default="*",             help='Jet category ID # jets, # b-jets: 3, 2 (0), 3,3 (1), 4,2 (2), 4,4 (3)')
parser.add_argument('--Channel', metavar='C', type=str,  default="combined",      help='Dilepton channels: ee, emu, mumu, combined')
parser.add_argument('--StepN',   metavar='S', type=str,  default="7",             help='Step number in analysis cutflow')
parser.add_argument('--Nominal', metavar='L', type=str,  default="Nominal",       help='Nominal, etc.')
parser.add_argument('--Normalized', metavar='L', type=str,  default="lumiCorrected", help='plain (nominal), or lumiCorrected')
parser.add_argument('--Merge',   metavar='M', type=bool, default=False,           help='Jet category merged to single table')

args = parser.parse_args()

# Load list of files to run over
fileName = str(args.DirPath)+"/"+str(args.Nominal)+"/"+str(args.Channel)+"/"+str(args.Normalized)+"_events_step"+str(args.StepN)+"_cate"+str(args.CateN)+".txt"
print fileName
fileList =  glob.glob(fileName)

# Set internal variables to use (such as category type)
categoryType = "cate"+str(args.CateN)
count = 0

list0 = []
list1 = []
list2 = []
list3 = []
list4 = []
list5 = []

cell = defaultdict(list)

# Regular expression designed to parse input text file (break each text line into columns for later access)
regexString = r'(\W*\w*\W*\w*\W*\w*\W*\w*\s*\W*\w*\W*\w*\W*\w*\W*\s*\w*\s*\w*\.*\W*):\s*(\W*\d*\.*\d*\s*)\+\-\s*(\d*.*\d*)\s*\((\d*.*\d*)\%\)'
testMatch = re.compile(regexString)


# Include latex header
print '\documentclass[8pt]{article} \n\
\usepackage[letterpaper,textwidth=8.0in,textheight=10.5in]{geometry} \n\
\usepackage{float}   \n\
\usepackage{amsmath} \n\
\usepackage{amssymb} \n\
\usepackage{lscape}  \n\
\usepackage{hyperref} \n\
\usepackage{color}    \n\
\usepackage{graphicx} \n\
\n\
\pagestyle{empty}  \n\
\n\
\\begin{document} \n'

# Iterate through list of files
for file in fileList:
	# Open a file
	fo = open(file, "r")

	pattern = categoryType
        m = re.search(pattern, file)

        if m != None:
		text = str(re.sub(r'\/', '\\\\\/', fo.name))

		print "\\begin{table}[!hbtp] \n\
\\centering \n\
\\caption["+fo.name+"]{"+str(re.sub(r'_', '\\\\_', text))+"} \n\
\\begin{tabular}{ l | l | l | l } \n\
\\hline \n\
\\hline \n\
Sample & Yield & Error & Uncertainty \\\\\n\
\\hline \n\
\\hline"

		# Read lines of files
		for line in fo:
			m = testMatch.match(line)
			tmp = str(re.sub(r'#', '\\\\', str(m.group(1))))
			mGroup1 = str(re.sub(r' ', '\\\\;', tmp))
			list0.append(mGroup1)
			
			# Following if statment included to break the table format from individual samples and the total yield
			if m.group(1) != "Total background":
				print ("$\\rm %-35s$\t&\t%10.10s & %-10s\t&\t$\pm$ %-10s \\\\" % (mGroup1, m.group(2), m.group(3), m.group(4)))
			else:
				print "\hline"
				print ("$\\rm %-35s$\t&\t%10.10s & %-10s\t&\t$\pm$ %-10s \\\\" % (mGroup1, m.group(2), m.group(3), m.group(4)))
				
			# Store event yield values into respective category 
			if args.Merge == True:
				if str(fo.name).find("cate0") != -1:
					cell["cate0"].append(m.group(2))
					list1.append(m.group(2))
				elif str(fo.name).find("cate1") != -1:
					cell["cate1"].append(m.group(2))
					list2.append(m.group(2))
				elif str(fo.name).find("cate2") != -1:
					cell["cate2"].append(m.group(2))
					list3.append(m.group(2))
				elif str(fo.name).find("cate3") != -1:
					cell["cate3"].append(m.group(2))
					list4.append(m.group(2))


		print "\\hline\n\
\hline\n\
\end{tabular}\n\
\end{table}"
		
		print "\n\n"
		count = count + 1

		# Clearing the latex page is necessary since adding to many tables sequentially can cause the latex compiler not to compile
		if count%15 == 0:
			print "\clearpage"
				
	# Close opend file
	fo.close()

if args.Merge == True and args.CateN == "*":
	# Latex header for jet category mereged table 
	print "\\begin{table}[!hbtp] \n\
\\centering \n\
\\caption[Event yields for the ee + $e\mu$+ $\mu\mu$ channel]{Event yields for the ee + $e\mu$+ $\mu\mu$ channel (analysis cutflow step "+str(args.StepN)+").}\n\
\\begin{tabular}{ l | l | l | l | l} \n\
\\hline \n\
\\hline \n\
Process sample & 3 jets, 2 b-jets & $\geq$~3 jets, 3 b-jets & $\geq$~4 jets, 2 b-jets & $\geq$~4 jets, $\geq$~4 b-jets \\\\\n\
\\hline"

	# Print out merged category table format 
	counter = 0

	for j in range(0,12):

		if list0[j] == "Total\;background":   
			print "\hline"
			
		print "$\\rm ",list0[j],"$"," & ",
			
		for i in range(0,4):
			if counter < 3:
				print cell["cate"+str(i)][j], " & ",
				counter = counter+1
			else:
				print cell["cate"+str(i)][j], " \\\\"
				counter = 0
		if list0[j] == "Pseudodata":
			print "\hline"


	print "\\hline\n\
\hline\n\
\end{tabular}\n\
\end{table}"

print "\n"
print "\end{document}\n"


