#!/bin/sh

#######################################
# Author: Christian Contreras         #
# Date:   Jan 11, 2013                #
# Email: chrisjcc@physics.rutgers.edu #
#######################################

# Description: Script moves these types CMSSW_*.stderr, CMSSW_*.stdout, Watchdog_*.log.gz, crab_fjr_*.xml 
# found in the res directory out of the way to facilate crab re-submission. 

input=$1
dir=$2
main=$PWD

if [[ $# -lt 2 ]]; then
    echo "Usage: ./crabJobXMLMover.sh \"5,44,66,100-105\" SingleMu_Run2012D-PromptReco-v1_AOD/runs_204599-205667"
    exit 1
fi

if [ ! -f $input ];then
    echo -e $input
    string=`echo $input | awk -F "," '{for(i=1; i <= NF; i++) printf "%s ",$i}'`
    
elif [ -s $input ];then
    cat $input
    string=`cat $input | awk -F "," '{for(i=1; i <= NF; i++) printf "%s ",$i}'`
fi

array=(${string})

for n in ${array[@]}
  do
  if [[ $n =~ "-" ]] ;then
      element=`echo $n | awk -F "-" '{for(i=$1; i <= $2; i++) printf "%s ",i}'`
      jobs+=($element)  
  else
      jobs+=($n)
  fi
done

for j in ${jobs[@]}
  do
  mkdir -p $main/$dir/res/tmp/
  mv $main/$dir/res/*_${j}.* $main/$dir/res/tmp/
done
