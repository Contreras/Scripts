Docomentation overview:

- The script EventYieldToLatexConverter.py converts text table format to latex format

- The script creatQandATwiki.py: Creates twiki formated page which can also reads in a .txt input file containing ARC, Convener, or CMS member analysis comments. The output will be read to include repsponses to comments and question give by publication commeette review documentation.

- The script publicTwiki.py: Creates a template twiki page for CMS public twiki containting analysis official plots to include comments as well and abstract. 

- The crabJobXMLMover.sh: Code usesage within cms is deprecated, but is included as example code for other projects.
